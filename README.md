# Legacy Preview SiteAccess Matcher Bundle
eZ Platform bundle which sets correct default siteaccess in legacy admin preview.

## Installation
- Run `composer require`:

```bash
$ composer require contextualcode/legacy-preview-siteaccess-matcher-bundle
```
- Enable this bundle in `ezpublish/EzPublishKernel.php` file by adding next line in `registerBundles` method:

```php
    public function registerBundles()
    {
        $bundles = array(
            ...
            new ContextualCode\LegacyPreviewSiteAccessMatcherBundle\ContextualCodeLegacyPreviewSiteAccessMatcherBundle()
        );
```
- Installs legacy extensions:

```
$ php ezpublish/console ezpublish:legacybundles:install_extensions --relative
```
- Regenerate eZ Publish Legacy autoloads:

```
$ php ezpublish/console ezpublish:legacy:script bin/php/ezpgenerateautoloads.php
```
- Done.
