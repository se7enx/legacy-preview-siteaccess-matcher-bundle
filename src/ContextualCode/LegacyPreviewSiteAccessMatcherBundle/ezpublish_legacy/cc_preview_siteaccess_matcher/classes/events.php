<?php
/**
 * @package contextualcode/legacy-preview-siteaccess-matcher-bundle
 * @author  Serhey Dolgushev <serhey@contextualcode.com>
 * @date    24 July 2018
 **/

class ccPreviewSiteAccessMatcherEvents
{
    public static function setVersionViewSiteAccess(eZURI $uri) {
        $originalUri = $uri->attribute('original_uri');
        if (strpos($originalUri, 'content/versionview/') !== 0) {
            return;
        }

        $objectId = (int) $uri->element(2, false);
        $version = (int) $uri->element(3, false);
        if ($objectId <= 0 || $version <= 0 || strpos($originalUri, '/site_access/') !== false) {
            return;
        }

        $http = eZHTTPTool::instance();
        if (count($_POST) === 0) {
            $matchingSiteAccess = self::getMatchingSiteAccess($objectId, $version);
            if ($matchingSiteAccess === null) {
                return;
            }

            $http->setPostVariable('ChangeSettingsButton', 'Update view');
            $http->setPostVariable('SelectedSiteAccess', $matchingSiteAccess);
            if (is_callable(array('ezxFormToken', 'getToken'))) {
                $http->setPostVariable(ezxFormToken::FORM_FIELD, ezxFormToken::getToken());
            }
        }
    }

    public static function getMatchingSiteAccess($objectId, $version) {
        $ini = new eZINI();
        $default_access = $ini->variable('SiteSettings', 'DefaultAccess');
        $object = eZContentObject::fetch($objectId);
        if ($object instanceof eZContentObject === false) {
            return $default_access;
        }

        $node = $object->attribute('main_node');
        if ($node instanceof eZContentObjectTreeNode === false) {
            $nodeAssignments = $object->version($version)->attribute('node_assignments');
            if (count($nodeAssignments) > 0) {
                $node = eZContentObjectTreeNode::fetch($nodeAssignments[0]->attribute('parent_node'));
            }
        }
        if ($node instanceof eZContentObjectTreeNode === false) {
            return $default_access;
        }

        $pathArray = array_reverse($node->attribute('path_array'));
        $rootNodeIds = eZINI::instance()->variable('SiteAccessSettings', 'RootNodeIDs');
        foreach ($pathArray as $pathNodeId) {
            foreach ($rootNodeIds as $siteAccess => $rootNodeId) {
                if ((int) $rootNodeId === (int) $pathNodeId && strpos($siteAccess, 'admin') === false) {
                    return $siteAccess;
                }
            }
        }

        return $default_access;
    }
}
