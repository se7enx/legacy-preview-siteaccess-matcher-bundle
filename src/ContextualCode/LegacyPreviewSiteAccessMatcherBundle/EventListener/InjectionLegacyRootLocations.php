<?php

namespace ContextualCode\LegacyPreviewSiteAccessMatcherBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use eZ\Publish\Core\MVC\Legacy\LegacyEvents;
use eZ\Publish\Core\MVC\Legacy\Event\PreBuildKernelEvent;
use Symfony\Component\Yaml\Parser;

class InjectionLegacyRootLocations implements EventSubscriberInterface
{
    protected $container;

    public function __construct($container) {
        $this->container = $container;
    }

    public function onBuildKernel(PreBuildKernelEvent $event) {
        $settings = array('site.ini/SiteAccessSettings/RootNodeIDs' => $this->getSitAccessRootLocations());
        $alreadyInjectedSettings = (array) $event->getParameters()->get('injected-settings');
        $settings = array_merge_recursive($alreadyInjectedSettings, $settings);
        $event->getParameters()->set('injected-settings', $settings);
    }

    protected function getSitAccessRootLocations() {
        $return = array();

        $configResolver = $this->container->get('ezpublish.config.resolver.core');
        $siteAccesses = $this->container->getParameter('ezpublish.siteaccess.list');
        foreach ($siteAccesses as $siteAccess) {
            $return[$siteAccess] = $configResolver->getParameter('content.tree_root.location_id', 'ezsettings', $siteAccess);
        }

        return $return;
    }

    public static function getSubscribedEvents() {
        return array(
            LegacyEvents::PRE_BUILD_LEGACY_KERNEL => array('onBuildKernel', 128),
        );
    }
}
